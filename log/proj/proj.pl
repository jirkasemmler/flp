/** FLP 2015/2016

Rubikova kostka 

Jiri Semmler, xsemml01

preklad: swipl -q -g start -o flp16-log -c input2.pl
*/


merge_list([],L,L ).
merge_list([H|T],L,[H|M]):-
    merge_list(T,L,M).

%% helpers
getnth([LIST_H|_], 0, LIST_H) :- !.
getnth([_|LIST_T], INT, RET) :- B is INT - 1,getnth(LIST_T, B, RET).

first(IN, OUT) :- getnth(IN,0,OUT). %% indexuju od nuly
second(IN, OUT) :- getnth(IN,1,OUT).
third(IN, OUT) :- getnth(IN,2,OUT).
fourth(IN, OUT) :- getnth(IN,3,OUT).
%% helpers
%% helpers




%Reads line from stdin, terminates on LF or EOF.
read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], !;
		read_line(LL,_),% atom_codes(C,[Cd]),
		[C|LL] = L).

%Tests if character is EOF or LF.
isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C,Code), Code==10).

read_lines(Ls) :-
	read_line(L,C),
	write(l  ),
	writeln(L),
	write(c  ),
	writeln(C),
	write(ls  ),
	writeln(Ls),
	( C == end_of_file, Ls = [] ;
	  read_lines(LLs), Ls = [L|LLs]
	).



% rozdeli radek na podseznamy
split_line([],[[]]) :- !.
split_line([' '|T], [[]|S1]) :- !, split_line(T,S1).
split_line([32|T], [[]|S1]) :- !, split_line(T,S1).    % aby to fungovalo i s retezcem na miste seznamu
split_line([H|T], [[H|G]|S1]) :- split_line(T,[G|S1]). % G je prvni seznam ze seznamu seznamu G|S1

% vstupem je seznam radku (kazdy radek je seznam znaku)
split_lines([],[]).
split_lines([L|Ls],[H|T]) :- split_lines(Ls,T), split_line(L,H).


% mam jako
% A
% B C D E
% F

preloz_kostku([
	[E1],
	[E2],
	[E3],
	[ A1, B1, C1, D1 ],
	[ A2, B2, C2, D2 ],
	[ A3, B3, C3, D3 ],
	[F1],
	[F2],
	[F3]
], [
	[A1,A2,A3],
	[B1,B2,B3],
	[C1,C2,C3],
	[D1,D2,D3],
	[E1,E2,E3],
	[F1,F2,F3]
]
).

rotace_vod_stred([
	[A1,A2,A3],
	[B1,B2,B3],
	[C1,C2,C3],
	[D1,D2,D3],
	[E1,E2,E3],
	[F1,F2,F3]
],[
	[A1,D2,A3],
	[B1,A2,B3],
	[C1,B2,C3],
	[D1,C2,D3],
	[E1,E2,E3],
	[F1,F2,F3]
]
).
rotace_vod_spodek([
	[A1,A2,A3],
	[B1,B2,B3],
	[C1,C2,C3],
	[D1,D2,D3],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
],[
	[A1,A2,D3],
	[B1,B2,A3],
	[C1,C2,B3],
	[D1,D2,C3],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_31,F_21,F_11],[F_32,F_22,F_12],[F_33,F_23,F_13]]
]
).

rotace_vod_vrch([
	[A1,A2,A3],
	[B1,B2,B3],
	[C1,C2,C3],
	[D1,D2,D3],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
],[
	[D1,A2,A3],
	[A1,B2,B3],
	[B1,C2,C3],
	[C1,D2,D3],
	[[E_31,E_21,E_11],[E_32,E_22,E_12],[E_33,E_23,E_13]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
]
).
rotace_svis_stred([
	[[A_11,A_12,A_13],[A_21,A_22,A_23],[A_31,A_32,A_33]],
	[[B_11,B_12,B_13],[B_21,B_22,B_23],[B_31,B_32,B_33]],
	[[C_11,C_12,C_13],[C_21,C_22,C_23],[C_31,C_32,C_33]],
	[[D_11,D_12,D_13],[D_21,D_22,D_23],[D_31,D_32,D_33]],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
],[
		[[A_11,F_12,A_13],[A_21,F_22,A_23],[A_31,F_32,A_33]],
	[[B_11,B_12,B_13],[B_21,B_22,B_23],[B_31,B_32,B_33]],
		[[C_11,E_32,C_13],[C_21,E_22,C_23],[C_31,E_12,C_33]],
	[[D_11,D_12,D_13],[D_21,D_22,D_23],[D_31,D_32,D_33]],
		[[E_11,A_12,E_13],[E_21,A_22,E_23],[E_31,A_32,E_33]],
		[[F_11,C_32,F_13],[F_21,C_22,F_23],[F_31,C_12,F_33]]
]
).

rotace_svis_prava([
	[[A_11,A_12,A_13],[A_21,A_22,A_23],[A_31,A_32,A_33]],
	[[B_11,B_12,B_13],[B_21,B_22,B_23],[B_31,B_32,B_33]],
	[[C_11,C_12,C_13],[C_21,C_22,C_23],[C_31,C_32,C_33]],
	[[D_11,D_12,D_13],[D_21,D_22,D_23],[D_31,D_32,D_33]],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
],[
	[[A_11,A_12,F_13],[A_21,A_22,F_23],[A_31,A_32,F_33]],
	[[B_31,B_21,B_11],[B_32,B_22,B_12],[B_33,B_23,B_13]],
	[[E_33,C_12,C_13],[E_23,C_22,C_23],[E_13,C_32,C_33]],
	[[D_11,D_12,D_13],[D_21,D_22,D_23],[D_31,D_32,D_33]],
	[[E_11,E_12,A_13],[E_21,E_22,A_23],[E_31,E_32,A_33]],
	[[F_11,F_12,C_11],[F_21,F_22,C_21],[F_31,F_32,C_31]]
]
).


rotace_svis_leva([
	[[A_11,A_12,A_13],[A_21,A_22,A_23],[A_31,A_32,A_33]],
	[[B_11,B_12,B_13],[B_21,B_22,B_23],[B_31,B_32,B_33]],
	[[C_11,C_12,C_13],[C_21,C_22,C_23],[C_31,C_32,C_33]],
	[[D_11,D_12,D_13],[D_21,D_22,D_23],[D_31,D_32,D_33]],
	[[E_11,E_12,E_13],[E_21,E_22,E_23],[E_31,E_32,E_33]],
	[[F_11,F_12,F_13],[F_21,F_22,F_23],[F_31,F_32,F_33]]
],[
	[[F_11,A_12,A_13],[F_21,A_22,A_23],[F_31,A_32,A_33]],
	[[B_11,B_12,B_13],[B_21,B_22,B_23],[B_31,B_32,B_33]],
	[[C_11,C_12,E_31],[C_21,C_22,E_21],[C_31,C_32,E_11]],
	[[D_13,D_23,D_33],[D_12,D_22,D_32],[D_11,D_21,D_31]],
	[[A_11,E_12,E_13],[A_21,E_22,E_23],[A_31,E_32,E_33]],
	[[C_33,F_12,F_13],[C_23,F_22,F_23],[C_13,F_32,F_33]]
]
).

vypis_kostku([
	[A1,A2,A3],
	[B1,B2,B3],
	[C1,C2,C3],
	[D1,D2,D3],
	[E1,E2,E3],
	[F1,F2,F3]
]) :- 
	writeln(E1),
	writeln(E2),
	writeln(E3),
	write(A1),	write(B1),	write(C1),	writeln(D1),
	write(A2),	write(B2),	write(C2),	writeln(D2),
	write(A3),	write(B3),	write(C3),	writeln(D3),
	writeln(F1),
	writeln(F2),
	writeln(F3)
. 

start :-
		prompt(_, ''),
		read_lines(LL),
		split_lines(LL,S),

		writeln(S),
		preloz_kostku(S,Y),
		writeln(original),
		vypis_kostku(Y),
		writeln(-------------),
		%% rotace_vod_stred(Y,X),
		%% writeln(rotovano-dole-vodorovne),
		%% vypis_kostku(X),
		%% writeln(-------------),
		%% rotace_vod_vrch(Y,W),
		%% writeln(rotovano-hore-vodorovne),
		%% vypis_kostku(W),
		%% writeln(-------------),
		rotace_svis_leva(Y,Q),
		writeln(rotovano-svis-leva),
		vypis_kostku(Q),

		writeln(-------------),


		halt.
		
last(L, [L]).
last(E, [_|L]) :- last(E, L).




/** prevede retezec na seznam atomu */
% pr.: string("12.35",S). S = ['1', '2', '.', '3', '5'].
retezec([],[]).
retezec([H|T],[C|CT]) :- atom_codes(C,[H]), retezec(T,CT).



/** prevede seznam cislic na cislo */
% pr.: cislo([1,2,'.',3,5],X). X = 12.35
cislo(N,X) :- cislo(N,0,X).
cislo([],F,F).
cislo(['.'|T],F,X) :- !, cislo(T,F,X,10).
cislo([H|T],F,X) :- FT is 10*F+H, cislo(T,FT,X).
cislo([],F,F,_).
cislo([H|T],F,X,P) :- FT is F+H/P, PT is P*10, cislo(T,FT,X,PT).

% existuje knihovni predikat number_chars(?Number, ?CharList)
% pr.: number_chars(12.35, ['1', '2', '.', '3', '5']).
