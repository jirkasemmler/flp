import System.Environment
import System.Exit
import Data.Char
import Data.List
import System.IO



getFile file 
	| file == "" = hGetContents stdin -- read from stdIN
	| otherwise = readFile file   -- ok, I have the file to read from

run [akce, file]  -- define action to run
	| akce == "-i" = runI file
	| akce == "-1" = runSimpleRules file
	| akce == "-2" = runCNF file
	| otherwise = error "wrong input parameters"

parse [] = error "wrong input parameters"
parse [x] = run [x,""] -- read from stdin
parse [x,y]
	| x `elem` ["-i", "-1","-2"] = run [x,y]
	| otherwise = error "wrong input parameters"

parse _ = error "wrong input parameters" -- whatever else


-- vvv helpers
-- splits string by delimiter to list of strings - from stackoverflow
split [] t del = [t]
split (a:l) t del = if a==del then (t:split l [] del) else split l (t++[a]) del
--- ^^^ helpers

-- main structure with working data
data Grammar = 
	Grammar 
	{
	terminals :: [String], -- list of terminals (a,b,c...)
	nonterminals :: [String], -- list of NONterminals (A,B,C,...)
	startSymbol :: String,
	rules :: [(String, String)]  -- rules . from S->AB does go (S,[AB]) structure
} deriving Show


prepareRulesToPrint (x:xs) = ((fst x) ++ "->" ++ (snd x) ):  prepareRulesToPrint xs
prepareRulesToPrint x = []

printInput grammar = do
	putStrLn (intercalate "," (nonterminals grammar))
	putStrLn (intercalate "," (terminals grammar))
	putStrLn (startSymbol grammar)
	putStrLn (intercalate "\n" (prepareRulesToPrint (rules grammar)))
	
	return ()

parseOneRule x = ([splited!!0!!0], splited!!1) -- make to tuple , splited!!0!!0 means get first char of the first one from the pair
	where splited = split x [] '>' -- ok, now I have it as  ["S-", "ab"] (from S->ab)
-- split by > because it can not split by longer string (just by the char)

parseRules (x:xs) = parseOneRule x : parseRules xs
parseRules x =  []

main = do
	arguments <- getArgs
	parse arguments -- run it!

	return ()

getGrammar rows = Grammar { 
								nonterminals=split (rows!!0) [] ',' , 
								terminals=split (rows!!1) [] ',' , 
								startSymbol=(head (split (rows!!2) [] ',')) ,
								rules= parseRules (drop 3 rows)
	}


runI file = do
	contents <- getFile file
	let rows = lines contents
	let grammar = getGrammar rows
	printInput grammar
	return ()

-- nonterminal = reseny      
-- ntlist = seznam nonterminalu (vsech, at je s cim porovnavat)
-- sezanm pravidel
-- nts_of_current_nt = nalezene nonterminaly na prave strane jako jednoducha pravidla
fce::String -> Grammar -> [(String, String)] -> String -> String

fce current_nt gramatika [] nts_of_current_nt = nts_of_current_nt

fce current_nt gramatika (rule:ruless) nts_of_current_nt = if (fst rule == current_nt) && ((snd rule) `elem` ntlist )
	then fce current_nt gramatika ruless ((snd rule)++(fce (snd rule) gramatika (rules gramatika) [])++nts_of_current_nt)
	else fce current_nt gramatika ruless nts_of_current_nt 
	where ntlist = (nonterminals gramatika)
--           foreach NTs  gramatika   vysledna dvojice [ (NT, [NT na prave strane z JP]) ]
getSimpleRules::[String] -> Grammar -> [(String,String)]  -> [(String,String)]
getSimpleRules [] _ ret = ret
getSimpleRules (x:xs) gramatika ret = getSimpleRules xs gramatika ((x, (fce x gramatika (rules gramatika) x)):ret)


eraseSimpleRules::[(String, String)] -> [String] -> [(String, String)] -> [(String, String)]
eraseSimpleRules [] _ out = out
eraseSimpleRules (rule:rules) ntlist out = if length (snd rule) == 1 &&  (snd rule) `elem` ntlist
	then eraseSimpleRules rules ntlist out
	else eraseSimpleRules rules ntlist (rule:out)

makeRuleFromRule::String -> (String, String) ->  (String, String) 
makeRuleFromRule left rule = (left, (snd rule))
--      nt        
getListForNonterm::String -> String -> [(String, String)] ->  [(String, String)]  ->  [(String, String)] 
getListForNonterm nt [] pravidla ret = ret
getListForNonterm nt (right_nonterm:right_nonterms) pravidla ret = getListForNonterm nt right_nonterms pravidla ((map (makeRuleFromRule nt) (filter (\x -> fst x == [right_nonterm]) pravidla))++ret)

handleSimleRules::[(String,String)] ->[(String, String)] -> [(String, String)] -> [(String, String)]
handleSimleRules [] _ ret = ret
handleSimleRules (x:xs) bezJedPrav ret = handleSimleRules xs bezJedPrav ret++(getListForNonterm (fst x) (snd x) bezJedPrav [])

runSimpleRules file = do
	contents <- getFile file
	let grammar = getGrammar (lines contents)

	let meta = handleSimleRules (getSimpleRules (nonterminals grammar) grammar []) (eraseSimpleRules  (rules grammar) (nonterminals grammar) []) []

	printInput Grammar { nonterminals=(nonterminals grammar), terminals=(terminals grammar), startSymbol=(startSymbol grammar), rules=meta	}

	return ()

changeRuleToCNF::(String, String) -> ([(String, String)], String)
changeRuleToCNF rule = ([(fst rule, (take 1 right)++"<"++dropRight++">" ), 
	                     (("<"++dropRight++">" ),dropRight)
	                     ], "<"++dropRight++">")
	where 
		right = snd rule
		dropRight = drop 1 right

-- S    -> ABC
-- S    -> A<BC>
-- <BC> -> BC
	   -- seznam pravidel IN  seznam pravidel OUT  neterm. IN  ||  pravidla out        neterm out  
makeCNF::[(String, String)] -> [(String, String)] -> [String] -> ([(String, String)], [String])
makeCNF [] outRules outNts = (outRules , outNts)
makeCNF (rule:rules) outRules outNts = if (((not (elem '<' right)) && (length right) >= 3 ))
	then makeCNF rules (outRules++(fst changed)) (appendNonTermin outNts (snd changed))
	else makeCNF rules (rule:outRules)           (appendNonTermin outNts (fst rule))           
	where 
		right = snd rule
		changed = changeRuleToCNF rule

checkRule::(String, String) -> Bool
checkRule rule = if ( ((not (elem '<' (snd rule))) && (length (snd rule) <= 2)) || (elem '<' (snd rule)) )
	then True
	else False
		-- where right = (snd rule)

appendNonTermin nonterms toAppend = if (not(elem toAppend nonterms)) then toAppend:nonterms else nonterms

appendNonTerminLists target [] = target
appendNonTerminLists target (src:srcs) = appendNonTerminLists (appendNonTermin target src) srcs

areAllRulesInCNF::[(String, String)]->Bool
areAllRulesInCNF [] = True
areAllRulesInCNF (rule:rules) = if ((checkRule rule) == False) then False else areAllRulesInCNF rules

doCNF::([(String, String)],[String]) -> ([(String, String)], [String])
doCNF (rules, nts) = if (areAllRulesInCNF rules) 
	then (rules, nts) 
	else doCNF (makeCNF rules [] nts)

-- out : ([pravidla], nonterminaly)
-- this is kind of magic...
--        pravidlo        NONterminaly   terminaly     out pravidla          
fixRule::(String, String) -> [String] -> [String] -> ([(String, String)], [String])
fixRule rule nonterminals terminals
	| (((elem '<' right) || (length right == 2)) && ([(right!!0)] `elem` terminals) && (not([(right!!1)] `elem` terminals))) = ([((fst rule), (right!!0):"'"++(drop 1 right)), (((right!!0):"'"),[(right!!0)])] , ((right!!0):"'"):nonterminals) -- A->aB
	| ((length right == 2) && ([(right!!1)] `elem` terminals) && (not([(right!!0)] `elem` terminals))) = ([((fst rule), (take 1 right)++((right!!1):"'")), (((right!!1):"'"),[(right!!1)])] , ((right!!1):"'"):nonterminals) -- A->Ba
	| ((length right == 2) && ([(right!!1)] `elem` terminals) && ([(right!!0)] `elem` terminals)) = ([((fst rule), (take 1 right)++"'"++(drop 1 right)++"'"), (((right!!0):"'"),[(right!!0)]), (((right!!1):"'"),[(right!!1)])] , [((right!!1):"'"),((right!!0):"'")]++nonterminals) -- A->ab ==> A->a'b' , a'->a , b'->b
	| otherwise = ([rule], nonterminals)

	where 
		right = snd rule

fixTerminalsInRules::[(String, String)] -> [String] -> [String] -> [(String, String)] -> ([(String, String)], [String])
fixTerminalsInRules [] nonterminals terminals rulesRet = (rulesRet, nonterminals)
fixTerminalsInRules (rule:rules) nonterminals terminals rulesRet = fixTerminalsInRules rules (appendNonTerminLists nonterminals (snd tmp)) terminals (rulesRet++(fst tmp))
	where 
		tmp = fixRule rule nonterminals terminals

runCNF file = do
	contents <- getFile file
	let grammar = getGrammar (lines contents)
	
	let fixedSimpleTerminals = fixTerminalsInRules (fst cnfRulesAndNonterms) (snd cnfRulesAndNonterms) (terminals grammar) []
		where
			cnfRulesAndNonterms = doCNF (withoutSR, (nonterminals grammar))
				where 
					withoutSR = handleSimleRules (getSimpleRules (nonterminals grammar) grammar []) (eraseSimpleRules  (rules grammar) (nonterminals grammar) []) []
	
	printInput Grammar {nonterminals=(snd fixedSimpleTerminals), terminals=(terminals grammar), startSymbol=(startSymbol grammar), rules=(fst fixedSimpleTerminals) }
	return ()

