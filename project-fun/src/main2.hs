import System.Environment
import System.Exit
import Data.Char
import Data.List
import System.IO

runSimpleRules file = print "gonna kill simple rules!"
runCNF file = print "I smell CNF in the air"

getFile file 
	| file == "" = hGetContents stdin -- read from stdIN
	| otherwise = readFile file   -- ok, I have the file to read from

run [akce, file]  -- define action to run
	| akce == "-i" = runI file
	| akce == "-1" = runSimpleRules file
	| akce == "-2" = runCNF file
	| otherwise = error "wrong input parameters"

parse [] = error "wrong input parameters"
parse [x] = run [x,""] -- read from stdin
parse [x,y]
	| x `elem` ["-i", "-1","-2"] = run [x,y]
	| otherwise = error "wrong input parameters"

parse _ = error "wrong input parameters" -- whatever else


-- vvv helpers
-- splits string by delimiter to list of strings - from stackoverflow
split [] t del = [t]
split (a:l) t del = if a==del then (t:split l [] del) else split l (t++[a]) del

getFirstFromTuple (a,_) = [a]
getSecondFromTuple (_,a) = a
--- ^^^ helpers

-- main structure with working data
data Grammar = 
	Grammar 
	{
	terminals :: [[Char]], -- list of terminals (a,b,c...)
	nonterminals :: [[Char]], -- list of NONterminals (A,B,C,...)
	startSymbol :: [[Char]],
	rules :: [(Char, [Char])]  -- rules . from S->AB does go (S,[AB]) structure
} deriving Show


prepareRulesToPrint (x:xs) = (getFirstFromTuple x ++ "->" ++ getSecondFromTuple x ):  prepareRulesToPrint xs
prepareRulesToPrint x = []

printInput grammar = do
	putStrLn (intercalate "," (terminals grammar))
	putStrLn (intercalate "," (nonterminals grammar))
	putStrLn (intercalate "\n" (startSymbol grammar))
	putStrLn (intercalate "\n" (prepareRulesToPrint (rules grammar)))
	
	return ()

parseOneRule x = (splited!!0!!0, splited!!1) -- make to tuple , splited!!0!!0 means get first char of the first one from the pair
	where splited = split x [] '>' -- ok, now I have it as  ["S-", "ab"] (from S->ab)
-- split by > because it can not split by longer string (just by the char)

parseRules (x:xs) = parseOneRule x : parseRules xs
parseRules x =  []

main = do
	arguments <- getArgs
	parse arguments -- run it!

	return ()

runI file = do
	contents <- getFile file
	let rows = lines contents
	let grammar = Grammar { 
								terminals=split (rows!!0) [] ',' , 
								nonterminals=split (rows!!1) [] ',' , 
								startSymbol=split (rows!!2) [] ',' ,
								rules= parseRules (drop 3 rows)
	}
	printInput grammar
	return ()