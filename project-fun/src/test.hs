import System.Environment
import System.Exit
import Data.Char
import Data.List
import System.IO

getFile file 
	| file == "" = hGetContents stdin -- read from stdIN
	| otherwise = readFile file   -- ok, I have the file to read from

getData file = do
	contents <- getFile file
	return $ lines contents